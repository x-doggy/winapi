#include <iostream>
#include <string>
#include <algorithm>
#include <windows.h>
#include <tchar.h>
using namespace std;

#if defined(UNICODE) || defined(_UNICODE)
#define _tcout wcout
#define _tcin  wcin
#define _tcerr wcerr
#define _tclog wclog
#else
#define _tcout cout
#define _tcin  cin
#define _tcerr cerr
#define _tclog clog
#endif

// Unicode to ASCII
string DecodeUnicode(const wstring &wstr) {
    if ( wstr.empty() ) return "";
    INT CONST iSizeNeeded = WideCharToMultiByte(1251, 0, &wstr[0], (INT) wstr.size(), NULL, 0, NULL, NULL);
    string strRes(iSizeNeeded, 0);
    WideCharToMultiByte(1251, 0, &wstr[0], (INT) wstr.size(), &strRes[0], iSizeNeeded, NULL, NULL);
    return strRes;
}

// ASCII to Unicode
wstring EncodeUnicode(const string &str) {
    if ( str.empty() ) return L"";
    INT CONST iSizeNeeded = MultiByteToWideChar(1251, 0, &str[0], (INT) str.size(), NULL, 0);
    wstring wstrRes(iSizeNeeded, 0);
    MultiByteToWideChar(1251, 0, &str[0], (INT) str.size(), &wstrRes[0], iSizeNeeded);
    return wstrRes;
}

int main(int argc, char **argv) {
    setlocale(LC_ALL, "Russian");

    // ASCII

    LPCSTR szAscii = "Ïðèâåò, ASCII Windows API!";
    cout << "[ASCII] ASCII string: " << szAscii << endl;
    UINT CONST uAsciiSize = strlen(szAscii);
    cout << "[ASCII] Length: " << uAsciiSize << endl;

    LPSTR lpszCopy = new CHAR[uAsciiSize + 1];
    strcpy(lpszCopy, szAscii);
    cout << "[ASCII] Copy string: " << lpszCopy << endl;

    UINT CONST N = strlen("Ïðèâåò, ASCII");
    strncpy(lpszCopy, szAscii, N);
    lpszCopy[N] = '\0';
    cout << "[ASCII] Copy string of n symbols: " << lpszCopy << endl;
    cout << "[ASCII] Copy substring: " << strstr(lpszCopy, "ASCII") << endl;
    cout << "[ASCII] Reverse copy string: " << _strrev(lpszCopy) << endl << endl;

    
    // UNICODE    
    const wchar_t *lpszUnicode = L"Hello, Unicode Windows API!";
    wcout << "[UNICODE] Unicode string: " << lpszUnicode << endl;
    const size_t uUnicodeSize = wcslen(lpszUnicode);
    wcout << "[UNICODE] Length: " << uUnicodeSize << endl;
    
    wchar_t *lpszUCopy = new wchar_t[uUnicodeSize + 1];
    wcscpy(lpszUCopy, lpszUnicode);
    wcout << "[UNICODE] Copy string: " << lpszUCopy << endl;
    
    const UINT M = 15;
    wcsncpy(lpszUCopy, lpszUnicode, M);
    lpszUCopy[M] = '\0';
    wcout << "[UNICODE] Copy string of m symbols: " << lpszUCopy << endl;
    wcout << "[UNICODE] Copy substring: " << wcsstr(lpszUCopy, L"Unicode") << endl;
    wcout << "[UNICODE] Reverse copy string: " << wcsrev(lpszUCopy) << endl << endl;
    

    // TCHAR
    
    const TCHAR *lpszTChar = _T("Hello, TCHAR Windows API!");
    _tcout << "[TCHAR] TCHAR string: " << lpszTChar << endl;
    const size_t uTCharSize = _tcslen(lpszTChar);
    _tcout << "[TCHAR] Length: " << uTCharSize << endl;

    TCHAR *lpszTCopy = new TCHAR[uTCharSize + 1];
    _tcscpy(lpszTCopy, lpszTChar);
    _tcout << "[TCHAR] Copy string: " << lpszTCopy << endl;

    const UINT K = 13;
    _tcsncpy(lpszTCopy, lpszTChar, K);
    lpszTCopy[K] = '\0';
    _tcout << "[TCHAR] Copy string of K symbols: " << lpszTCopy << endl;
    _tcout << "[TCHAR] Copy substring: " << _tcsstr(lpszTCopy, _T("TCHAR")) << endl;
    _tcout << "[TCHAR] Reverse copy string: " << _tcsrev(lpszTCopy) << endl << endl;


    // UNICODE to ASCII

    char *lpszCopyCopy = new char[uAsciiSize + 1];
    strcpy(lpszCopyCopy, lpszCopy);

    int nSuccessBytes;
    nSuccessBytes = WideCharToMultiByte(1251, 0, wcsrev(lpszUCopy), -1, &lpszCopy[0], uAsciiSize, NULL, NULL);
    if (nSuccessBytes > 0) {
        cout << "[UNICODE to ASCII]: " << lpszCopy << endl;
    }


    // ASCII to UNICODE

    nSuccessBytes = MultiByteToWideChar(1251, 0, strrev(lpszCopyCopy), -1, &lpszUCopy[0], uUnicodeSize);
    if (nSuccessBytes > 0) {
        wcout << "[ASCII to UNICODE]: " << lpszUCopy << endl << endl;
    }
    
    
    // WSTRING
    
    wstring cppWideString = L"Hello, C++ Unicode string Windows API!";
    wcout << "[WSTRING] C++ WString: " << cppWideString << endl;
    wcout << "[WSTRING] Length: " << cppWideString.size() << endl;
    
    wstring cppWideStringCopy(cppWideString);
    wcout << "[WSTRING] Copy string: " << cppWideStringCopy << endl;
    
    const UINT H = 25;
    cppWideStringCopy.resize(H);
    wcout << "[WSTRING] Copy string of h symbols: " << cppWideStringCopy << endl;
    size_t uFound = cppWideStringCopy.find_first_of(L"string");
    if (uFound != wstring::npos) {
        wcout << "[WSTRING] Copy substring: " << cppWideStringCopy << endl;
    }
    reverse(cppWideStringCopy.begin(), cppWideStringCopy.end());
    wcout << "[WSTRING] Reverse wstring: " << cppWideStringCopy << endl;
    cout << "[STRING] C++ string: " << string_cast<string>(cppWideStringCopy) << endl;

    return 0;
}
