#pragma once

#ifndef __ALGEBRA_HPP__
#define __ALGEBRA_HPP__

#ifdef ALGEBRA_EXPORTS
#define DLL_SPEC __dllspec(dllimport)
#else
#define DLL_SPEC __dllspec(dllexport)
#endif

#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>

namespace Algebra {
    typedef int data_t;
    
    struct vector {
        data_t x, y, z;
        vector const add(vector const &v) {
            return { x + v.x, y + v.y, z + v.z };
        }
        data_t const scalarProduct(vector const &v) {
            return x * v.x + y * v.y + z * v.z;
        }
        vector const vectorProduct(vector const &v1, vector const &v2) {
            return { v1.y * v2.z - v1.z * v2.y
                   , v1.z * v2.x - v1.x * v2.z
                   , v1.x * v2.y - v1.y * v2.x };
        }
        inline void print(std::ostream &out = std::cout) {
            out << "(" << x << "; " << y << "; " << z << ")" << std::endl;
        }
    };

    // No incapsulation!
    struct matrix {
        data_t **m_pMatrix;
        size_t m_size;
        matrix(size_t const size, int const val = 0) : m_size(size) {
            m_pMatrix = new data_t * [m_size];
            for (size_t i=0; i<m_size; i++)
                m_pMatrix[i] = new data_t[m_size];
            for (size_t i=0; i<m_size; i++)
                for (size_t j=0; j<m_size; j++)
                    m_pMatrix[i][j] = val;
        }
        ~matrix() {
            for (size_t i=0; i<m_size; i++)
                delete[] m_pMatrix[i];
            delete[] m_pMatrix;
        }
        matrix* copy(matrix const &m) {
            return new matrix(*this);
        }
        matrix(matrix const &m) : m_size(m.m_size) {
            m_pMatrix = new data_t * [m_size];
            for (size_t i=0; i<m_size; i++)
                m_pMatrix[i] = new data_t[m_size];
            for (size_t i=0; i<m_size; i++)
                for (size_t j=0; j<m_size; j++)
                    m_pMatrix[i][j] = m.m_pMatrix[i][j];
        }
    };

    matrix addMatrix(matrix const &m1, matrix const &m2) {
        matrix res(m1.m_size);
        for (size_t i=0; i<m1.m_size; i++)
            for (size_t j=0; j<m1.m_size; j++)
                res.m_pMatrix[i][j] = m1.m_pMatrix[i][j] + m2.m_pMatrix[i][j];
        return res;
    }

    matrix mulMatrix(matrix const &m1, matrix const &m2) {
        matrix res(m1.m_size);
        for (size_t i=0; i<m1.m_size; i++) {
            for (size_t j=0; j<m1.m_size; j++) {
                res.m_pMatrix[i][j] = 0;
                for (size_t k=0; k<m1.m_size; k++) {
                    res.m_pMatrix[i][j] += m1.m_pMatrix[i][k] * m2.m_pMatrix[k][j];
                }
            }
        }
        return res;
    }

    void inverseMatrix(matrix &m) {
        data_t temp;
        matrix E(m.m_size);
        size_t const SIZE = m.m_size;

        for (size_t i=0; i<SIZE; i++) {
            for (size_t j=0; j<SIZE; j++) {
                E.m_pMatrix[i][j] = i == j ? 1 : 0;
            }
        }
        for (size_t k=0; k<SIZE; k++) {
            temp = m.m_pMatrix[k][k];
            for (size_t j=0; j<SIZE; j++) {
                m.m_pMatrix[k][j] /= temp;
                E.m_pMatrix[k][j] /= temp;
            }
            for (size_t i=k + 1; i<SIZE; i++) {
                temp = m.m_pMatrix[i][k];
                for (size_t j=0; j<SIZE; j++) {
                    m.m_pMatrix[i][j] -= m.m_pMatrix[k][j] * temp;
                    E.m_pMatrix[i][j] -= E.m_pMatrix[k][j] * temp;
                }
            }
        }
        for (size_t k=SIZE - 1; k>0; --k) {
            for (size_t i = k-1; i>=0; --i) {
                temp = m.m_pMatrix[i][k];
                for (size_t j=0; j<SIZE; j++) {
                    m.m_pMatrix[i][j] -= m.m_pMatrix[k][j] * temp;
                    E.m_pMatrix[i][j] -= E.m_pMatrix[k][j] * temp;
                }
            }
        }
        for (size_t i=0; i<SIZE; i++) {
            for (size_t j=0; j<SIZE; j++) {
                m.m_pMatrix[i][j] = E.m_pMatrix[i][j];
            }
        }
    }
    
    inline void print(matrix const &m, std::ostream &out = std::cout) {
        for (size_t i=0; i<m.m_size; i++) {
            for (size_t j=0; j<m.m_size; j++)
                out << m.m_pMatrix[i][j] << "  ";
            out << std::endl;
        }
    }
}

#endif
