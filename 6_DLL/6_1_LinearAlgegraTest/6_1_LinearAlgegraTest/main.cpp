#include "Algebra.hpp"
#pragma comment(lib, "Algebra.lib")
using namespace Algebra;

int main(int argc, char **argv) {
    vector a = { 1,  2,  3};
    vector b = {-1, -2, -3};
    vector c;
    (c = a.add(b)).print();
    
    matrix m1(3, 2);
    matrix m2(3, 3);
    print(m1);
    print(m2);
    inverseMatrix(m2);
    print(m2);

    getchar();
    return 0;
}
