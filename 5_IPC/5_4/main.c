#include <windows.h>
#include <process.h>
#include <time.h>
#include <stdio.h>

HANDLE hSemWriter, hSemReader, hEvent;

UINT WINAPI PushBook(void *lpvParam) {
    srand(time(0));
    while (TRUE) {
        if ( WAIT_OBJECT_0 != WaitForSingleObject(hSemReader, INFINITE) )
            return FALSE;

        int *nBook = (int*) lpvParam;
        *nBook += 1;

        puts("New book is on table!");
        Sleep(150);

        if ( !ReleaseSemaphore(hSemWriter, 1, NULL) ) {
            fprintf(stderr, "ReleaseSemaphore error: %lu\n", GetLastError());
        }

        if (rand() % 20 == 0) {
            SetEvent(hEvent);
            break;
        }

    }
    return TRUE;
}


UINT WINAPI PopBook(void *lpvParam) {
    while (TRUE) {
        if (WAIT_OBJECT_0 != WaitForSingleObject(hSemWriter, INFINITE))
            return FALSE;

        int *nBook = (int*) lpvParam;
        *nBook += 1;

        puts("Nice book!");
        Sleep(150);

        if ( !ReleaseSemaphore(hSemReader, 1, NULL) ) {
            fprintf(stderr, "ReleaseSemaphore error: %lu\n", GetLastError());
        }

        if ( WAIT_OBJECT_0 == WaitForSingleObject(hEvent, 0) ) {
            break;
        }
    }

    return TRUE;
}


int main(int argc, char **argv) {
    int nBook = 0;
    int const THREADS_COUNT = 2;

    hEvent     = CreateEvent(NULL, TRUE, FALSE, NULL);
    hSemReader = CreateSemaphore(NULL, 1, 1, NULL);
    hSemWriter = CreateSemaphore(NULL, 0, 1, NULL);

    if (hSemReader == NULL || hSemWriter == NULL) {
        fprintf(stderr, "CreateSemaphore error: %lu\n", GetLastError());
        return -1;
    }

    HANDLE thWriter = (HANDLE) _beginthreadex(NULL, 0, PushBook, &nBook, 0, NULL);
    HANDLE thReader = (HANDLE) _beginthreadex(NULL, 0, PopBook, &nBook, 0, NULL);

    HANDLE hRdWrThreads[] = { thWriter, thReader };

    if ( WAIT_OBJECT_0 == WaitForMultipleObjects(THREADS_COUNT, hRdWrThreads, TRUE, INFINITE) ) {
        puts("Read / Write procedure completed!");
    }

    return 0;
}

