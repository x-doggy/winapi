#include <windows.h>
#include <stdio.h>

int main(int argc, char **argv) {
    CONST CHAR szMutexName[] = "56e33145-v_stadnik_v-a13f-424d-9b6a-43e5c5613c8f"; // Unique name of mutex!
    HANDLE hMutex;

    hMutex = CreateMutex(NULL, FALSE, szMutexName);
    if (hMutex != NULL && ERROR_ALREADY_EXISTS == GetLastError()) {
        fprintf(stderr, "%s\n", "Error! Another instance of this app is running!");
        CloseHandle(hMutex);
        return -1;
    }
    puts("This message prints only one time!");
    CloseHandle(hMutex);

    return 0;
}
