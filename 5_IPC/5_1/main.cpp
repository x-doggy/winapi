#include "ThreadSafeLinkedList.hpp"
#include <cstdio>

ThreadSafeLinkedList list;

UINT __stdcall Thread1(LPVOID lpvParam) {
    puts("Thread 1 begins...");
    list.push_back(5);
    list.print();
    list.push_front(10);
    list.print();
    list.push_front(20);
    list.print();
    list.push_back(25);
    list.print();
    list.push_front(30);
    list.print();
    list.pop_back();
    list.print();
    list.push_back(35);
    list.print();
    list.pop_front();
    list.print();
    cout << endl << endl;
    return 1;
}

UINT __stdcall Thread2(LPVOID lpvParam) {
    puts("Thread 2 begins...");
    list.push_back(1);
    list.print();
    list.push_front(11);
    list.print();
    list.push_front(21);
    list.print();
    list.push_back(26);
    list.print();
    list.push_front(31);
    list.print();
    list.pop_back();
    list.print();
    list.push_back(36);
    list.print();
    list.pop_front();
    list.print();
    cout << endl << endl;
    return 2;
}

UINT __stdcall Thread3(LPVOID lpvParam) {
    puts("Thread 3 begins...");
    list.push_back(2);
    list.print();
    list.push_front(12);
    list.print();
    list.push_front(22);
    list.print();
    list.push_back(27);
    list.print();
    list.push_front(32);
    list.print();
    list.pop_back();
    list.print();
    list.push_back(37);
    list.print();
    list.pop_front();
    list.print();
    cout << endl << endl;
    return 3;
}

int main(int argc, char **argv) {
    const unsigned int N = 3;
    HANDLE hThreads[N];
    UINT uiThreadIDs[N];

    hThreads[0] = (HANDLE) _beginthreadex(NULL, 0, &Thread1, NULL, 0, &uiThreadIDs[0]);
    hThreads[1] = (HANDLE) _beginthreadex(NULL, 0, &Thread2, NULL, 0, &uiThreadIDs[1]);
    hThreads[2] = (HANDLE) _beginthreadex(NULL, 0, &Thread3, NULL, 0, &uiThreadIDs[2]);

    if (WAIT_OBJECT_0 == WaitForMultipleObjects(N, hThreads, TRUE, INFINITE)) {
        puts("Threads done!");
    }

    CloseHandle(hThreads[2]);
    CloseHandle(hThreads[1]);
    CloseHandle(hThreads[0]);

    return 0;
}
