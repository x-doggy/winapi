#ifndef __THREAD_SAFE_LINKED_LIST__
#define __THREAD_SAFE_LINKED_LIST__

#include <windows.h>
#include <process.h>
#include <iostream>
using namespace std;


/*
 * -------------------------------------------------
 * �������� �������� �����`
 * -------------------------------------------------
 */
typedef int DATA;
typedef DATA *LPDATA;
typedef CONST DATA CDATA;
typedef CONST LPDATA CPDATA;


/*
 * -------------------------------------------
 * ��������� ���� ������. � ����������!
 * -------------------------------------------
 */
typedef struct _node_ {
    _node_ *next, *prev;
    DATA    data;

    _node_()
        : next(nullptr)
        , prev(nullptr)
        , data(DATA())
    { }
    _node_(_node_ *thePrev, _node_ *theNext, CDATA theData = 0)
        : next(theNext)
        , prev(thePrev)
        , data(theData)
    { }
    _node_(CDATA theData)
        : next(nullptr)
        , prev(nullptr)
        , data(theData)
    { }
} NODE, *LPNODE;


/*
 * -------------------------------------------
 * "������ ���������": ���������������, ������
 * -------------------------------------------
 */
class ThreadSafeLinkedList {
public:

    /*
     * -----------------
     * �������� � ������
     * -----------------
     */
    class Iterator {
    public:
        explicit Iterator(const LPNODE pList) : m_pCurr(pList) { }

        inline CDATA operator*() const {
            return m_pCurr->data;
        }

        inline CPDATA operator->() const {
            return &(m_pCurr->data);
        }
        
        Iterator& operator++() {
            m_pCurr = m_pCurr->next;
            return *this;
        }

        Iterator operator++(int) {
            Iterator it(*this);
            m_pCurr = m_pCurr->next;
            return it;
        }

        Iterator& operator--() {
            m_pCurr = m_pCurr->prev;
            return *this;
        }

        Iterator operator--(int) {
            Iterator it(*this);
            m_pCurr = m_pCurr->prev;
            return it;
        }

        inline bool operator==(const Iterator &it) {
            return m_pCurr == it.m_pCurr;
        }

        inline bool operator!=(const Iterator &it) {
            return !operator==(it);
        }

    private:
        LPNODE m_pCurr;
    };
    

    ThreadSafeLinkedList()
        : m_pHead(nullptr)
        , m_pTail(nullptr)
        , m_size(0)
    {
        InitializeCriticalSection(&m_cs);
    }

    virtual ~ThreadSafeLinkedList() {
        clear();
        DeleteCriticalSection(&m_cs);
    }

    inline bool empty() const {
        return m_size == 0;
    }

    inline bool alone() const {
        return m_size == 1;
    }

    void clear() {
        for (LPNODE it = m_pHead; it->next != m_pTail; it=it->next) {
            pop_back();
        }
    }

    void pop_front() {
        LPNODE pOld = m_pHead;
        EnterCriticalSection(&m_cs);
        if (!alone()) {
            m_pHead = m_pHead->next;
            m_pHead->prev = nullptr;
        } else {
            delete m_pHead;
            m_pHead = m_pTail = nullptr;
        }
        --m_size;
        LeaveCriticalSection(&m_cs);
        delete pOld;
    }
    
    void pop_back() {
        LPNODE pOld = m_pTail;
        EnterCriticalSection(&m_cs);
        if (!alone()) {
            m_pTail = m_pTail->prev;
            m_pTail->next = nullptr;
        } else {
            delete m_pHead;
            m_pHead = m_pTail = nullptr;
        }
        --m_size;
        LeaveCriticalSection(&m_cs);
        delete pOld;
    }
    
    void push_front(CDATA data = DATA()) {
        EnterCriticalSection(&m_cs);
        if (!empty()) {
            LPNODE pNewNode = new NODE(nullptr, m_pHead, data);
            m_pHead = m_pHead->prev = pNewNode;
        } else {
            m_pTail = m_pHead = new NODE(data);
        }
        ++m_size;
        LeaveCriticalSection(&m_cs);
    }

    void push_back(CDATA data = DATA()) {
        EnterCriticalSection(&m_cs);
        if (!empty()) {
            LPNODE pNewNode = new NODE(m_pTail, nullptr, data);
            m_pTail = m_pTail->next = pNewNode;
        } else {
            m_pTail = m_pHead = new NODE(data);
        }
        ++m_size;
        LeaveCriticalSection(&m_cs);
    }
    
    Iterator begin() const {
        return Iterator(m_pHead);
    }

    Iterator end() const {
        return Iterator(m_pTail);
    }

    size_t const size() const {
        return m_size;
    }
    
    void print(ostream &out = cout) {
        EnterCriticalSection(&m_cs);
        for (Iterator it = begin(); it != end(); ++it) {
            out << *it << "  ";
        }
        out << endl;
        LeaveCriticalSection(&m_cs);
    }
    
    friend ostream& operator<<(ostream &out, const ThreadSafeLinkedList &list);
    
protected:
    LPNODE m_pHead, m_pTail;
    size_t m_size;
    CRITICAL_SECTION m_cs;
};

ostream& operator<<(ostream &out, ThreadSafeLinkedList &list) {
    list.print(out);
    return out;
}


/*
 * ------------------------------
 * ��������� ���� �������� ������
 * ------------------------------
 */
CONST DWORD ThreadGetExitCode(HANDLE hThread) {
    DWORD dwExitCode;
    GetExitCodeThread(hThread, &dwExitCode);
    return dwExitCode;
}

#endif
