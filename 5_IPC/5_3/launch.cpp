#include <windows.h>
#include <process.h>
#include <stdio.h>

int main(int argc, char **argv) {
    char szArg[2][5] = { "1234", "5678" };
    STARTUPINFO si1 = { 0 };
    si1.cb = sizeof(si1);
    STARTUPINFO si2 = { 0 };
    si2.cb = sizeof(si2);

    PROCESS_INFORMATION pi1 = { 0 };
    if (CreateProcess("main.exe", szArg[0], NULL, NULL, FALSE, NULL, NULL, NULL, &si1, &pi1)) {
        puts("Process 1 created!");
        Sleep(100);
    }
    PROCESS_INFORMATION pi2 = { 0 };
    if (CreateProcess("main.exe", szArg[1], NULL, NULL, FALSE, NULL, NULL, NULL, &si2, &pi2)) {
        puts("Process 2 created!");
    }
    if (WAIT_OBJECT_0 == WaitForSingleObject(pi1.hProcess, INFINITE)
      &&
        WAIT_OBJECT_0 == WaitForSingleObject(pi2.hProcess, INFINITE)) {
        CloseHandle(pi1.hThread);
        CloseHandle(pi2.hThread);
        CloseHandle(pi1.hProcess);
        CloseHandle(pi2.hProcess);
    }

    return 0;
}
