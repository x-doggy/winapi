#include <windows.h>
#include <iostream>
using namespace std;


struct Mapping {
private:
    char *lpBufAddr;
    static const DWORD cdwBasePageSize = 1 << 20;
    static const DWORD cdwMapViewSize  = 1 << 16;

    DWORD dwWritten;
    DWORD dwMapped;
    HANDLE hMapFile;
    HANDLE hFile;
    HANDLE hMutex;

public:
    Mapping(const char * const cszFileName)
        : dwWritten(0)
        , dwMapped(cdwMapViewSize)
    {
        hFile = CreateFile(cszFileName, GENERIC_READ | GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, 0, NULL);
        DWORD dwSize = GetFileSize(hFile, 0);
        hMapFile = CreateFileMapping(hFile, NULL, PAGE_READWRITE, 0, cdwBasePageSize, NULL);
        if (dwSize > 0) {
            DWORD dwViewSize = (dwSize + (cdwBasePageSize - 1)) / cdwBasePageSize * cdwBasePageSize;
            lpBufAddr = (char *) MapViewOfFile(hMapFile, FILE_MAP_READ | FILE_MAP_WRITE, 0, 0, dwViewSize);
            dwWritten = dwSize - 1;
        } else {
            lpBufAddr = (char *) MapViewOfFile(hMapFile, FILE_MAP_READ | FILE_MAP_WRITE, 0, 0, cdwMapViewSize);
        }
        hMutex = CreateMutex(NULL, FALSE, "47336345-d8f2-41c5-9e42-4f34058e5dae");
    }

    ~Mapping() {
        UnmapViewOfFile(lpBufAddr);
        CloseHandle(hMapFile);
        CloseHandle(hFile);
        CloseHandle(hMutex);
    }

    bool write(const char *szSource) {
        size_t uLength = strlen(szSource);
        if (WAIT_OBJECT_0 == WaitForSingleObject(hMutex, INFINITE)) {
            if (dwWritten + uLength <= dwMapped) {
                strcpy(lpBufAddr + dwWritten, szSource);
                (lpBufAddr + dwWritten)[uLength + 1] = '\0';
            } else {
                if (dwMapped + cdwMapViewSize <= cdwBasePageSize) {
                    UnmapViewOfFile(lpBufAddr);
                    lpBufAddr = (char *) MapViewOfFile(hMapFile, FILE_MAP_READ | FILE_MAP_WRITE, 0, 0, dwMapped + cdwMapViewSize);
                    strcpy(lpBufAddr + dwWritten, szSource);
                    (lpBufAddr + dwWritten)[uLength + 1] = '\0';
                    dwMapped += cdwMapViewSize;
                } else {
                    return false;
                }
            }
            dwWritten += uLength + 1;
        }
        ReleaseMutex(hMutex);
        return true;
    }
};


int main(int argc, char **argv) {
    if (argc != 2) {
        fprintf(stderr, "Usage: \"%s\" <textfile>\n", argv[0]);
        getchar();
        return -1;
    }

    char const szFileName[] = "test.txt";
    Mapping map(szFileName);
    for (int i=0; i < 10E3; i++) {
        map.write(argv[1]);
    }

    return 0;
}

