#include <windows.h>
#include <iostream>
#include <list>
#include <iterator>
using namespace std;

struct ListData {
    list<int> &lst;
    int low, high;
    ListData(list<int> aList, int const Low, int const High)
        : lst(aList), low(Low), high(High) { }
};

UINT WINAPI ListThread(void *lpvParam) {
    ListData *data = (ListData *) lpvParam;
    for (; data->low < data->high; ++data->low) {
        data->lst.push_back(data->low);
    }
    copy(data->lst.begin(), data->lst.end(), ostream_iterator<int>(cout, " "));
    cout << endl;
    return 0;
}

int main(int argc, char **argv) {
    int const SIZE = 100;
    int const THREADS = 8;
    HANDLE hThreads[THREADS];
    list<int> lst;

    for (int i=0; i<THREADS; i++) {
        Sleep(150);
        int const k = i * SIZE / THREADS;
        int const j = (i + 1) * SIZE / THREADS;
        ListData const data(lst, k, j);
        hThreads[i] = (HANDLE) _beginthreadex(NULL, 0, ListThread, (void *) &data, 0, NULL);
    }

    if (WAIT_OBJECT_0 == WaitForMultipleObjects(THREADS, hThreads, TRUE, INFINITE)) {
        cout << endl << "Threads work finished!" << endl;
    } else {
        cerr << endl << "We cannot wait!" << endl;
    }

    for (int i=0; i<THREADS; i++) {
        CloseHandle(hThreads[i]);
    }

    return 0;
}
