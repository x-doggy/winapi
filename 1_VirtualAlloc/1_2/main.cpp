#include <iostream>
#include <windows.h>
using namespace std;

typedef int DATA;
typedef DATA *LPDATA;

class Matrix {
public:
    Matrix(UINT sz) : m_uSize(sz)
                    , m_pBase( (LPDATA) VirtualAlloc(NULL, m_uSize * m_uSize * sizeof(DATA), MEM_RESERVE, PAGE_READWRITE) )
    {}

    ~Matrix() {
        VirtualFree(m_pBase, 0, MEM_RELEASE);
    }

    void setValue(UINT i, UINT j, DATA value) {
        LPDATA pElem = getValueAddr(i, j);
        LPDATA pPageStart = (LPDATA)((int)(pElem) & ~(getPageSize() - 1));
        __try {
            *pElem = value;
        }
        __except ((GetExceptionCode() == EXCEPTION_ACCESS_VIOLATION) ? EXCEPTION_EXECUTE_HANDLER : EXCEPTION_CONTINUE_SEARCH) {
            VirtualAlloc(pPageStart, getPageSize(), MEM_COMMIT, PAGE_READWRITE);
            *pElem = value;
        }
    }

    DATA getValue(UINT i, UINT j) const {
        LPDATA pElem = getValueAddr(i, j);
        LPDATA pPageStart = (LPDATA)((int)(pElem) & ~(getPageSize() - 1));
        DATA dataRetValue = 0;
        __try {
            dataRetValue = *pElem;
        }
        __except ((GetExceptionCode() == EXCEPTION_ACCESS_VIOLATION) ? EXCEPTION_EXECUTE_HANDLER : EXCEPTION_CONTINUE_EXECUTION) {
        }
        return dataRetValue;
    }

private:
    DATA        *m_pBase;
    UINT const   m_uSize;

    static DWORD getPageSize() {
        SYSTEM_INFO si;
        GetSystemInfo(&si);
        return si.dwPageSize;
    }

    inline LPDATA getValueAddr(UINT i, UINT j) const {
        return m_pBase + i * m_uSize + j;
    }
};

int main(int argc, char **argv) {
    Matrix m(10000);
    m.setValue(10, 15, 6000);
    cout << "(1) " << m.getValue(10, 15) << endl;

    m.setValue(5000, 401, -7854);
    cout << "(2) " << m.getValue(5000, 401) << endl;

    m.setValue(9895, 9895, 9999);
    cout << "(3) " << m.getValue(9895, 9895) << endl;

    m.setValue(9999, 9999, -19999);
    cout << "(4) " << m.getValue(9999, 9999) << endl;

    m.setValue(9999, 9999, 56);
    cout << "(5) " << m.getValue(9999, 9999) << endl;

    cout << "(6) " << m.getValue(600, 500) << endl;

    return 0;
}
