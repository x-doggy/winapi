#include <windows.h>
#include <iostream>
using namespace std;


struct Mapping {
private:
    char *lpBufAddr;
    static const DWORD cdwBasePageSize = 1 << 20;
    static const DWORD cdwMapViewSize  = 1 << 16;
    
    DWORD dwWritten;
    DWORD dwMapped;
    HANDLE hMapFile;
    HANDLE hFile;

public:
    Mapping(const char * const cszFileName)
        : dwWritten(0)
        , dwMapped(cdwMapViewSize)
    {
        hFile = CreateFile(cszFileName, GENERIC_READ | GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, 0, NULL);
        DWORD dwSize = GetFileSize(hFile, 0);
        hMapFile = CreateFileMapping(hFile, NULL, PAGE_READWRITE, 0, cdwBasePageSize, NULL);
        if (dwSize > 0) {
            DWORD dwViewSize = (dwSize + (cdwBasePageSize - 1)) / cdwBasePageSize * cdwBasePageSize;
            lpBufAddr = (char *) MapViewOfFile(hMapFile, FILE_MAP_READ | FILE_MAP_WRITE, 0, 0, dwViewSize);
            dwWritten = dwSize - 1;
        } else {
            lpBufAddr = (char *) MapViewOfFile(hMapFile, FILE_MAP_READ | FILE_MAP_WRITE, 0, 0, cdwMapViewSize);
        }
    }

    ~Mapping() {
        UnmapViewOfFile(lpBufAddr);
        CloseHandle(hMapFile);
        CloseHandle(hFile);
    }

    bool write(const char *szSource) {
        size_t uLength = strlen(szSource);
        if (dwWritten + uLength <= dwMapped) {
            strcpy(lpBufAddr + dwWritten, szSource);
            (lpBufAddr + dwWritten)[uLength + 1] = '\0';
        } else {
            if (dwMapped + cdwMapViewSize <= cdwBasePageSize) {
                UnmapViewOfFile(lpBufAddr);
                lpBufAddr = (char *) MapViewOfFile(hMapFile, FILE_MAP_READ | FILE_MAP_WRITE, 0, 0, dwMapped + cdwMapViewSize);
                strcpy(lpBufAddr + dwWritten, szSource);
                (lpBufAddr + dwWritten)[uLength + 1] = '\0';
                dwMapped += cdwMapViewSize;
            } else {
                return false;
            }
        }
        dwWritten += uLength + 1;
        return true;
    }

    void reverse(const char * const szSource){
        UnmapViewOfFile(lpBufAddr);
        lpBufAddr = (char *) MapViewOfFile(hMapFile, FILE_MAP_READ | FILE_MAP_WRITE, 0, 0, 0);
        size_t uLength = strlen(szSource);
        char *szElem = new char[uLength];
        DWORD dwReversed = 0;
        while (dwReversed < dwWritten) {
            szElem = lpBufAddr + dwReversed;
            _strrev(szElem);
            dwReversed += uLength + 1;
        }
    }
};


int main(int argc, char **argv) {
    const char szFileName[] = "test.txt";
    Mapping map(szFileName);
    map.write("poiuytrewq987654321");
    map.write("Vladimir");
    map.reverse(szFileName);
    
    getchar();
    return 0;
}

