#include <windows.h>
#include <stdio.h>
#include <locale.h>

inline CONST UINT BytesToMbytes(UINT b) {
    return b / 1024 / 1024;
}

int main(int argc, char **argv) {
    setlocale(LC_ALL, "Russian");

    MEMORYSTATUSEX ms = { 0 };
    ms.dwLength = sizeof(ms);

    // Get global memory status
    GlobalMemoryStatusEx(&ms);

    puts("--------------------------------------------");
    puts("| ���������� � ���������� ��������� ������ |");
    puts("--------------------------------------------");
    printf("%30s %u%%\n"    , "�������� ������:"      , ms.dwMemoryLoad);
    printf("%30s %u �����\n", "����� ���. ������:"    , BytesToMbytes(ms.ullTotalPhys));
    printf("%30s %u �����\n", "����� ����. ������:"   , BytesToMbytes(ms.ullTotalVirtual));
    printf("%30s %u �����\n", "�������� ���. ������:" , BytesToMbytes(ms.ullAvailPhys));
    printf("%30s %u �����\n", "�������� ����. ������:", BytesToMbytes(ms.ullAvailVirtual));
    printf("%30s %u �����\n", "����� Page File:"      , BytesToMbytes(ms.ullTotalPageFile));
    printf("%30s %u �����\n", "�������� Page File:"   , BytesToMbytes(ms.ullAvailPageFile));
    printf("%30s %u �����\n", "������� ����. ����. �:", BytesToMbytes(ms.ullAvailExtendedVirtual));

    return 0;
}
