#include <iostream>
#include <list>
#include <algorithm>
#include <windows.h>
using namespace std;

typedef int DATA;
typedef DATA *LPDATA;

// ============  M A T R I X  ============

class Matrix {
public:
    Matrix(UINT sz) : m_uSize(sz)
                    , m_pBase ( (LPDATA) VirtualAlloc(NULL, m_uSize * m_uSize * sizeof(DATA), MEM_RESERVE, PAGE_READWRITE) )
    {}

    ~Matrix() {
        VirtualFree(m_pBase, 0, MEM_RELEASE);
    }

    VOID setValue(UINT i, UINT j, DATA value) {
        LPDATA pElem = getValueAddr(i, j);
        LPDATA pPageStart = (LPDATA)( (int)(pElem)  & ~(getPageSize() - 1) );
        auto it = find(m_memList.begin(), m_memList.end(), pPageStart);
        if (it == m_memList.end()) {
            // not found
            VirtualAlloc(pPageStart, getPageSize(), MEM_COMMIT, PAGE_READWRITE);
            m_memList.push_back(pPageStart);
        }
        *pElem = value;
    }

    DATA getValue(UINT i, UINT j) const {
        LPDATA pElem = getValueAddr(i, j);
        LPDATA pPageStart = (LPDATA)( (int)(pElem)  & ~(getPageSize() - 1) );
        auto it = find(m_memList.begin(), m_memList.end(), pPageStart);
        return (it != m_memList.end()) ? *pElem : 0;
    }

    LPINT getMatrixAddr() const {
        return m_pBase;
    }

    VOID printMem() const {
        for (auto v : m_memList)
            cout << v << " -> ";
        cout << endl;
    }

private:
    UINT const   m_uSize;
    DATA        *m_pBase;
    list<LPVOID> m_memList;

    static DWORD getPageSize() {
        SYSTEM_INFO si;
        GetSystemInfo(&si);
        return si.dwPageSize;
    }

    inline LPDATA getValueAddr(UINT i, UINT j) const {
        return m_pBase + i * m_uSize + j;
    }
};

int main(int argc, char **argv) {
    Matrix m(10000);
    cout << "(1) ";
    m.printMem();

    m.setValue(10, 15, 6000);
    cout << "(2)";
    m.printMem();

    cout << m.getValue(10, 15) << endl;
    cout << "(3)";
    m.printMem();

    m.setValue(5000, 401, -7854);
    cout << m.getValue(5000, 401) << endl;
    cout << "(4)";
    m.printMem();

    m.setValue(9895, 9895, 9999);
    cout << "(5)";
    m.printMem();

    m.setValue(9999, 9999, -19999);
    cout << "(6)";
    m.printMem();

    m.setValue(9999, 9999, 56);
    cout << "(7)";
    m.printMem();
    cout << m.getValue(9999, 9997) << endl;

    return 0;
}
