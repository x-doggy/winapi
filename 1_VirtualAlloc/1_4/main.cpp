#include <windows.h>
#include <iostream>
#include <list>
#include <algorithm>
#include <fstream>
#include "VMQuery.h"
using namespace std;

typedef int DATA;
typedef DATA *LPDATA;


// ============  M A T R I X  ============

class Matrix {
public:
    Matrix(UINT sz) : m_uSize(sz)
                    , m_pBase ( (LPDATA) VirtualAlloc(NULL, m_uSize * m_uSize * sizeof(DATA), MEM_RESERVE, PAGE_READWRITE) )
    {}

    ~Matrix() {
        VirtualFree(m_pBase, 0, MEM_RELEASE);
    }

    VOID setValue(UINT i, UINT j, DATA value) {
        LPDATA pElem = getValueAddr(i, j);
        LPDATA pPageStart = (LPDATA)( (int)(pElem)  & ~(getPageSize() - 1) );
        auto it = find(m_memList.begin(), m_memList.end(), pPageStart);
        if (it == m_memList.end()) {
            // not found
            VirtualAlloc(pPageStart, getPageSize(), MEM_COMMIT, PAGE_READWRITE);
            m_memList.push_back(pPageStart);
        }
        *pElem = value;
    }

    DATA getValue(UINT i, UINT j) const {
        LPDATA pElem = getValueAddr(i, j);
        LPDATA pPageStart = (LPDATA)( (int)(pElem)  & ~(getPageSize() - 1) );
        auto it = find(m_memList.begin(), m_memList.end(), pPageStart);
        return (it != m_memList.end()) ? *pElem : 0;
    }

    LPINT getMatrixAddr() const {
        return m_pBase;
    }

    VOID printMem() const {
        for (auto v : m_memList)
            cout << v << " -> ";
        cout << endl;
    }

private:
    UINT const   m_uSize;
    DATA        *m_pBase;
    list<LPVOID> m_memList;

    static DWORD getPageSize() {
        SYSTEM_INFO si;
        GetSystemInfo(&si);
        return si.dwPageSize;
    }

    inline LPDATA getValueAddr(UINT i, UINT j) const {
        return m_pBase + i * m_uSize + j;
    }
};



// ============ P P O C E S S   I N F O  ============

typedef struct _block_info_ {
    PVOID pvBlkBaseAddress;
    DWORD dwBlkProtection;
    SIZE_T BlkSize;
    DWORD dwBlkStorage;
} BlkInfo, *pBlkInfo;

typedef struct _region_info_ {
    PVOID pvRgnBaseAddress;
    DWORD dwRgnProtection;
    SIZE_T RgnSize;
    DWORD dwRgnStorage;
    DWORD dwRgnBlocks;
    DWORD dwRgnGuardBlks;
    BOOL fRgnIsAStack;
} RgnInfo, *pRgnInfo;

// These two structs are result of split VMQUERY struct



BlkInfo getBlkInfo(const VMQUERY *vmq) {
    BlkInfo info;
    info.BlkSize          = vmq->BlkSize;
    info.dwBlkProtection  = vmq->dwBlkProtection;
    info.dwBlkStorage     = vmq->dwBlkStorage;
    info.pvBlkBaseAddress = vmq->pvBlkBaseAddress;
    return info;
}

RgnInfo getRgnInfo(const VMQUERY *vmq) {
    RgnInfo info;
    info.pvRgnBaseAddress = vmq->pvBlkBaseAddress;
    info.dwRgnBlocks      = vmq->dwRgnBlocks;
    info.dwRgnGuardBlks   = vmq->dwRgnGuardBlks;
    info.dwRgnProtection  = vmq->dwRgnProtection;
    info.fRgnIsAStack     = vmq->fRgnIsAStack;
    info.RgnSize          = vmq->RgnSize;
    return info;
}

VOID printBlkInfo(const BlkInfo &blkInfo, ostream &out = cout) {
    out << "Block base address: " << blkInfo.pvBlkBaseAddress << endl
        << "Block size: " << blkInfo.BlkSize << endl
        << "Block protection: " << blkInfo.dwBlkProtection << endl
        << "Block storage: " << blkInfo.dwBlkStorage << endl;
}

VOID printRgnInfo(CONST RgnInfo &regInfo, ostream &out = cout) {
    out << "Region base address: " << regInfo.pvRgnBaseAddress << endl
        << "Region size: " << regInfo.RgnSize << endl
        << "Region protection: " << regInfo.dwRgnProtection << endl;
}

VOID printBlkInfoList(const list<BlkInfo> &blkList, ostream &out = cout) {
    for (auto v : blkList) {
        cout << endl;
        printBlkInfo(v, out);
    }
}

VOID printRgnInfoList(const list<RgnInfo> &rgnList, ostream &out = cout) {
    for (auto v : rgnList) {
        cout << endl;
        printRgnInfo(v, out);
    }
}

list<BlkInfo> getBlkInfoList(HANDLE hProcess, LPCVOID pvAddress, CONST Matrix &m) {
    list<BlkInfo> BlkInfoList;
    VMQUERY vmq = { 0 };
    BOOL bOk = TRUE;

    while (bOk) {
        bOk = VMQuery(hProcess, pvAddress, &vmq);

        if (bOk) {
            for (DWORD dwBlock = 0; bOk && (dwBlock < vmq.dwRgnBlocks); dwBlock++) {
                pvAddress = ((PBYTE)pvAddress + vmq.BlkSize);
                if (dwBlock < vmq.dwRgnBlocks - 1) {
                    bOk = VMQuery(hProcess, pvAddress, &vmq);
                    if (bOk) BlkInfoList.push_back(getBlkInfo(&vmq));
                }
            }
        }

        CONST LPINT pMatrixAddress = m.getMatrixAddr();
        pvAddress = ((PBYTE)vmq.pvRgnBaseAddress + vmq.RgnSize);

        if (pMatrixAddress == pvAddress) {
            MEMORY_BASIC_INFORMATION mbi;
            VirtualQueryEx(GetCurrentProcess(), pMatrixAddress, &mbi, sizeof(mbi));
        }

        cout << "Matrix address: " << pMatrixAddress << endl
             << "Address: " << pvAddress << endl
             << "Region size: " << vmq.RgnSize << endl;
    }
    return BlkInfoList;
}

list<BlkInfo> getBlkInfoList(HANDLE hProcess, LPCVOID pvAddress = NULL) {
    list<BlkInfo> BlkInfoList;
    VMQUERY vmq = { 0 };
    BOOL bOk = TRUE;

    while (bOk) {
        bOk = VMQuery(hProcess, pvAddress, &vmq);
        if (bOk) {
            for (DWORD dwBlock = 0; bOk && (dwBlock < vmq.dwRgnBlocks); dwBlock++) {
                pvAddress = ((PBYTE)pvAddress + vmq.BlkSize);
                if (dwBlock < vmq.dwRgnBlocks - 1) {
                    bOk = VMQuery(hProcess, pvAddress, &vmq);
                    if (bOk) BlkInfoList.push_back(getBlkInfo(&vmq));
                }
            }
        }
        pvAddress = ((PBYTE)vmq.pvRgnBaseAddress + vmq.RgnSize);
    }
    return BlkInfoList;
}

list<RgnInfo> getRgnInfoList(HANDLE hProcess, LPCVOID pvAddress = NULL) {
    list<RgnInfo> rgnList;
    VMQUERY vmq = { 0 };
    BOOL bOk = TRUE;

    while (bOk) {
        bOk = VMQuery(hProcess, pvAddress, &vmq);
        pvAddress = ((PBYTE)vmq.pvRgnBaseAddress + vmq.RgnSize);
        rgnList.push_back(getRgnInfo(&vmq));
    }
    return rgnList;
}

int main(int argc, char **argv) {
    list<RgnInfo> beforeRgnList, afterRgnList;
    HANDLE hProcess = GetCurrentProcess();
    ofstream fout;

    fout.open("before.txt");
    beforeRgnList = getRgnInfoList(hProcess);
    printRgnInfoList(beforeRgnList, fout);
    fout.close();

    Matrix m(1);

    fout.open("after.txt");
    afterRgnList = getRgnInfoList(hProcess);
    printRgnInfoList(afterRgnList, fout);
    fout.close();

    return 0;
}
