#include <iostream>
#include <windows.h>
#include <tchar.h>
using namespace std;

#if defined(UNICODE) || defined(_UNICODE)
#define _tcout wcout
#define _tcin  wcin
#define _tcerr wcerr
#define _tclog wclog
#else
#define _tcout cout
#define _tcin  cin
#define _tcerr cerr
#define _tclog clog
#endif

class BigBuffer {
public:
    BigBuffer()
        : m_lpszBaseBuffer( (LPTSTR) VirtualAlloc(NULL, HOW_MUCH_RESERVE, MEM_RESERVE, PAGE_READWRITE) )
        , m_nFirstNonCommited(0)
        , m_nLastWrited(0)
    { }

    ~BigBuffer() {
        VirtualFree(m_lpszBaseBuffer, 0, MEM_RELEASE);
    }

    VOID addString(LPCTSTR lpszInString) {
        if (m_nFirstNonCommited >= HOW_MUCH_RESERVE) {
            _tcerr << "Warning! Cannot commit memory greater than reserved!" << endl;
            return;
        }
        if ((INT) _tcslen(lpszInString) >= m_nFirstNonCommited - m_nLastWrited) {
            if (VirtualAlloc(m_lpszBaseBuffer + m_nFirstNonCommited, _64K * sizeof(TCHAR), MEM_COMMIT, PAGE_READWRITE) == NULL) {
                _tclog << GetLastError() << endl;
                return;
            }
            m_nFirstNonCommited += _64K * sizeof(TCHAR);
        }

        _tcscpy(m_lpszBaseBuffer + m_nLastWrited, lpszInString);
        m_nLastWrited += _tcslen(lpszInString) + 1;
    }

    VOID print() const {
        LPTSTR lpszTmpBuffer = m_lpszBaseBuffer;
        for (; lpszTmpBuffer[0] != '\0'; lpszTmpBuffer += _tcslen(lpszTmpBuffer) + 1)
            _tcout << lpszTmpBuffer << endl;
    }

private:
    LPTSTR m_lpszBaseBuffer;
    INT    m_nFirstNonCommited;
    INT    m_nLastWrited;
    static INT CONST _64K             = 1 << 16;
    static INT CONST _1M              = 1 << 20;
    static INT CONST HOW_MUCH_RESERVE = _1M * sizeof(TCHAR);
};

int main(int argc, char **argv) {
    BigBuffer buf;
    for (INT i = 0; i < 800; i++) {
        buf.addString(_T("This is a very very biiiig buffer of constant string"));
        buf.addString(_T("Oh, no this is very biiiiiiiiiiggggggg string for biiiiiiig buffer again!!!!!"));
    }
    buf.print();

    return 0;
}
