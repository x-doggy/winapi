#include <windows.h>
#include <stdio.h>
#include <process.h>

UINT cnt;

UINT __stdcall ThreadFunc(LPVOID lpvParam) {
    puts("Second thread begins...");
    while (cnt < 1000000) {
        cnt++;
    }
    return 1;
}

int main(int argc, char **argv) {
    HANDLE hThread;
    UINT uiThreadID;

    puts("Creating second thread...");
    hThread = (HANDLE) _beginthreadex(NULL, 0, &ThreadFunc, NULL, 0, &uiThreadID);
    if (WAIT_OBJECT_0 == WaitForSingleObject(hThread, INFINITE)) {
        printf("Counter should be 1000000; It is %u\n\n", cnt);
    }

    DWORD dwExitThreadCode;
    GetExitCodeThread(hThread, &dwExitThreadCode);
    printf("Exit code of thread: %u\n", dwExitThreadCode);

    CloseHandle(hThread);

    return 0;
}
