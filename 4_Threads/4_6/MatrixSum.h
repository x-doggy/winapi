#ifndef __MATRIX_SUM__
#define __MATRIX_SUM__

#include <stdio.h>
#include <windows.h>
#include <process.h>

#define M_SIZE 10000

typedef struct _matrix_sum_ {
      INT **pMatrix;
    LPINT   pResultVector;
} MATRIXSUM, *LPMATRIXSUM;

typedef struct _matrix_seg_ {
    MATRIXSUM ms;
    DWORD lowBorder;
    DWORD highBorder;
} MATRIXSEG, *LPMATRIXSEG;


inline VOID printTimeDiff(DWORD dwStart, DWORD dwFinish, ostream &out = cout) {
    out << "Spend " << (dwFinish - dwStart) << " ms" << endl << endl;
    printf("Spend %lu ms\n\n", (dwFinish - dwStart));
}

INT **allocAndFill(INT **matrix) {
    matrix = new LPINT[M_SIZE];
    for (UINT i = 0; i < M_SIZE; i++) {
        matrix[i] = new int[M_SIZE];
        for (UINT j = 0; j < M_SIZE; j++) {
            matrix[i][j] = 1;
        }
    }
    return matrix;
}

VOID freeMatrix(INT **matrix) {
    for (UINT i = 0; i < M_SIZE; i++) {
        delete[] matrix[i];
    }
    delete[] matrix;
    matrix = NULL;
}

typedef void(WINAPI *ThreadFunc)(PTP_CALLBACK_INSTANCE, LPVOID, PTP_WORK);

void WINAPI ThreadpoolRows(PTP_CALLBACK_INSTANCE func, LPVOID lpvParam, PTP_WORK work) {
    LPMATRIXSEG matrix = (LPMATRIXSEG) lpvParam;
    for (UINT i = matrix->lowBorder; i < matrix->highBorder; i++) {
        for (UINT j = 0; j < M_SIZE; j++) {
            matrix->ms.pResultVector += matrix->ms.pMatrix[i][j];
        }
    }
}

void WINAPI ThreadpoolColumns(PTP_CALLBACK_INSTANCE func, LPVOID lpvParam, PTP_WORK work) {
    LPMATRIXSEG matrix = (LPMATRIXSEG) lpvParam;
    for (UINT i = matrix->lowBorder; i < matrix->highBorder; i++) {
        for (UINT j = 0; j < M_SIZE; j++) {
            matrix->ms.pResultVector += matrix->ms.pMatrix[j][i];
        }
    }
}

LPINT getMatrixSumByRowMultiThread(LPMATRIXSUM mas, ThreadFunc func) {
    SYSTEM_INFO si;
    GetSystemInfo(&si);
    CONST UINT cdwProcessorCores = si.dwNumberOfProcessors;
    printf("Number of cores = %d\n", cdwProcessorCores);

    MATRIXSEG mseg;
    mseg.ms = *mas;

    const int nWorks = cdwProcessorCores / 2;
    PTP_WORK *works = new PTP_WORK[nWorks];

    if (M_SIZE % cdwProcessorCores == 0) {
        UINT CONST dwMatrixSegmentSize = M_SIZE / cdwProcessorCores;
        DWORD CONST dwTimeStart = GetTickCount();
        for (INT i = 0; i < nWorks; i++) {
            mseg.lowBorder = i * dwMatrixSegmentSize;
            mseg.highBorder = mseg.lowBorder + dwMatrixSegmentSize;
            works[i] = CreateThreadpoolWork(func, (LPVOID) &mseg, NULL);
            SubmitThreadpoolWork(works[i]);
        }
        //Sleep(10);
        for (INT i = 0; i < nWorks; i++) {
            WaitForThreadpoolWorkCallbacks(works[i], FALSE);
            CloseThreadpoolWork(works[i]);
        }
        CONST DWORD dwTimeFinish = GetTickCount();
        printTimeDiff(dwTimeStart, dwTimeFinish);
    }

    delete[] works;
    return mseg.ms.pResultVector;
}

#endif
