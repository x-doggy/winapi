#include "MatrixSum.h"

int main(int argc, char **argv) {
    LPINT *pMatrix = nullptr;
    MATRIXSUM ms = { 0 };
    ms.pMatrix = allocAndFill(pMatrix);
    ms.pResultVector = new int[M_SIZE];

    cout << "Launch \"Get matrix sum by row (multi-thread)\"" << endl;
    ms.pResultVector = getMatrixSumByRowMultiThread(&ms, ThreadpoolRows);

    cout << "Launch \"Get matrix sum by column (multi-thread)\"" << endl;
    ms.pResultVector = getMatrixSumByRowMultiThread(&ms, ThreadpoolColumns);

    //freeMatrix(ms.pMatrix);
    //delete[] ms.pResultVector;

    return 0;
}
