#include "MatrixSum.h"

int main(int argc, char **argv) {
    LPINT *pMatrix = NULL;
    MATRIXSUM ms = { 0 };
    ms.pMatrix = allocAndFill(pMatrix);
    ms.pResultVector = new int[M_SIZE];

    puts("Launching task \"Sum by columns\"...");
    DWORD dwTimeStart = GetTickCount();
    sumByColumns(&ms);
    DWORD dwTimeFinish = GetTickCount();
    puts("Thread \"sumByColumnsEx\" finished!");
    printTimeDiff(dwTimeStart, dwTimeFinish);

    puts("Launching task \"Sum By rows\"...");
    dwTimeStart = GetTickCount();
    sumByRows(&ms);
    dwTimeFinish = GetTickCount();
    puts("Thread \"sumByRows\" finished!");
    printTimeDiff(dwTimeStart, dwTimeFinish);

    puts("Launching both tasks...");
    dwTimeStart = GetTickCount();
    sumByColumns(&ms);
    sumByRows(&ms);
    dwTimeFinish = GetTickCount();
    puts("Multiple tasks finished!");
    printTimeDiff(dwTimeStart, dwTimeFinish);

    delete[] ms.pResultVector;
    freeMatrix(ms.pMatrix);

    return 0;
}
