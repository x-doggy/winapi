#ifndef __MATRIX_SUM__
#define __MATRIX_SUM__

#include <stdio.h>
#include <windows.h>
#include <process.h>
#define M_SIZE 10000

typedef struct _matrix_sum_ {
      INT **pMatrix;
    LPINT   pResultVector;
} MATRIXSUM, *LPMATRIXSUM;

typedef struct _matrix_seg_ {
    MATRIXSUM ms;
    DWORD lowBorder;
    DWORD highBorder;
} MATRIXSEG, *LPMATRIXSEG;


inline VOID printTimeDiff(DWORD dwStart, DWORD dwFinish) {
    printf("Spend %lu ms\n\n", (dwFinish - dwStart));
}

INT **allocAndFill(INT **matrix) {
    matrix = new LPINT[M_SIZE];
    for (UINT i = 0; i < M_SIZE; i++) {
        matrix[i] = new INT[M_SIZE];
        for (UINT j = 0; j < M_SIZE; j++) {
            matrix[i][j] = 1;
        }
    }
    return matrix;
}

VOID freeMatrix(INT **matrix) {
    for (UINT i = 0; i < M_SIZE; i++) {
        delete[] matrix[i];
    }
    delete[] matrix;
    matrix = NULL;
}

UINT __stdcall sumByColumns(LPVOID lpvParam) {
    LPMATRIXSUM matrix = (LPMATRIXSUM) lpvParam;
    for (UINT i = 0; i < M_SIZE; i++) {
        for (UINT j = 0; j < M_SIZE; j++) {
            matrix->pResultVector[j] += matrix->pMatrix[i][j];
        }
    }
    return 0;
}

UINT __stdcall sumByRows(LPVOID lpvParam) {
    LPMATRIXSUM matrix = (LPMATRIXSUM) lpvParam;
    DWORD dwLocalSum = 0;
    for (UINT i = 0; i < M_SIZE; i++) {
        dwLocalSum = 0;
        for (UINT j = 0; j < M_SIZE; j++) {
            dwLocalSum += matrix->pMatrix[i][j];
        }
        matrix->pResultVector[i] = dwLocalSum;
    }
    return 0;
}

#endif
