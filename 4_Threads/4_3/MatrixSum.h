#ifndef __MATRIX_SUM__
#define __MATRIX_SUM__

#include <stdio.h>
#include <windows.h>
#include <process.h>

#define M_SIZE 10000

typedef struct _matrix_sum_ {
      INT **pMatrix;
    LPINT   pResultVector;
} MATRIXSUM, *LPMATRIXSUM;

typedef struct _matrix_seg_ {
    MATRIXSUM ms;
    DWORD lowBorder;
    DWORD highBorder;
} MATRIXSEG, *LPMATRIXSEG;


inline VOID printTimeDiff(DWORD dwStart, DWORD dwFinish) {
    printf("Spend %lu ms\n\n", (dwFinish - dwStart));
}

INT **allocAndFill(INT **matrix) {
    matrix = new LPINT[M_SIZE];
    for (UINT i = 0; i < M_SIZE; i++) {
        matrix[i] = new INT[M_SIZE];
        for (UINT j = 0; j < M_SIZE; j++) {
            matrix[i][j] = 1;
        }
    }
    return matrix;
}

VOID freeMatrix(INT **matrix) {
    for (UINT i = 0; i < M_SIZE; i++) {
        delete[] matrix[i];
    }
    delete[] matrix;
    matrix = NULL;
}

UINT __stdcall segSumByRows(LPVOID lpvParam) {
    LPMATRIXSEG matrix = (LPMATRIXSEG) lpvParam;
    for (UINT i = matrix->lowBorder; i < matrix->highBorder; i++) {
        for (UINT j = 0; j < M_SIZE; j++) {
            matrix->ms.pResultVector += matrix->ms.pMatrix[i][j];
        }
    }
    return 0;
}

UINT __stdcall segSumByColumns(LPVOID lpvParam) {
    LPMATRIXSEG matrix = (LPMATRIXSEG) lpvParam;
    for (UINT i = matrix->lowBorder; i < matrix->highBorder; i++) {
        for (UINT j = 0; j < M_SIZE; j++) {
            matrix->ms.pResultVector += matrix->ms.pMatrix[j][i];
        }
    }
    return 0;
}

typedef UINT(__stdcall *ThreadFunc)(LPVOID);

LPINT getMatrixSumByRowMultiThread(LPMATRIXSUM mas, ThreadFunc func) {
    SYSTEM_INFO si;
    GetSystemInfo(&si);
    CONST UINT cdwProcessorCores = si.dwNumberOfProcessors;
    printf("Number of cores = %lu\n", cdwProcessorCores);

    MATRIXSEG mseg;
    mseg.ms = *mas;

    if (M_SIZE % cdwProcessorCores == 0) {
        HANDLE *handles = new HANDLE[cdwProcessorCores];
        UINT CONST dwMatrixSegmentSize = M_SIZE / cdwProcessorCores;
        CONST DWORD dwTimeStart = GetTickCount();
        for (UINT i = 0; i < cdwProcessorCores; i++) {
            mseg.lowBorder = i * dwMatrixSegmentSize;
            mseg.highBorder = mseg.lowBorder + dwMatrixSegmentSize;
            handles[i] = (HANDLE) _beginthreadex(NULL, 0, func, (LPVOID) &mseg, 0, NULL);
        }
        if (WAIT_OBJECT_0 == WaitForMultipleObjects(cdwProcessorCores, handles, TRUE, INFINITE)) {
            puts("OK");
            delete[] handles;
            CONST DWORD dwTimeFinish = GetTickCount();
            printTimeDiff(dwTimeStart, dwTimeFinish);
        } else {
            fprintf(stderr, "Nope! Errcode = %lu\n", GetLastError());
        }
    }
    return mseg.ms.pResultVector;
}

#endif
