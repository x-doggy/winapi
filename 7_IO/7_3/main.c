#include <windows.h>
#include <tchar.h> 
#include <stdio.h>

void RecursiveSearch(LPSTR szPath) {
    WIN32_FIND_DATA wfd;
    HANDLE hFind;
    LPSTR const lpLastChar = szPath + strlen(szPath);
    strcat(szPath, "*");
    hFind = FindFirstFile(szPath, &wfd);
    *lpLastChar = '\0';
    if (INVALID_HANDLE_VALUE == hFind) {
        printf("FindFirstFile failed; Last error = %lu\n", GetLastError());
        getchar();
        return;
    }
    do {
        if (!strcmp(wfd.cFileName, ".") || !strcmp(wfd.cFileName, "..")) {
            continue;
        }
        strcat(szPath, wfd.cFileName);
        if (wfd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
            strcat(szPath, "\\");
            RecursiveSearch(szPath);
        } else {
            printf("%s\n", szPath);
        }
        *lpLastChar = '\0';
    }
    while (FindNextFile(hFind, &wfd));
    FindClose(hFind);
}

int main(int argc, char **argv) {
    char szFolder[512] = "C:\\Program Files\\";
    RecursiveSearch(szFolder);
    getchar();
    return 0;
}
