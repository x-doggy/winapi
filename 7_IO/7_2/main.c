#include <windows.h> 
#include <stdio.h> 

int main(int argc, char **argv) {
    int const N = 10;
    int const M = 1024;
    char const FILENAME[] = "test.txt";

    HANDLE hFile = CreateFile(FILENAME, GENERIC_WRITE, FILE_SHARE_WRITE, NULL, OPEN_ALWAYS, FILE_FLAG_OVERLAPPED, NULL);
    if (INVALID_HANDLE_VALUE == hFile) {
        fprintf(stderr, "%s\n", "CreateFile error");
        getchar();
        return -1;
    }
    
    char arr[N][M];
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < M; j++) {
            arr[i][j] = (char) (i + 48);
        }
    }

    OVERLAPPED ov[N] = { 0 };
    HANDLE hEvents[N];
    for (int i = 0; i < N; i++) {
        ov[i].hEvent = CreateEvent(0, TRUE, FALSE, 0);
        ov[i].Offset = (DWORD) (1024 * i);
        hEvents[i] = ov[i].hEvent;
        WriteFile(hFile, arr[i], M, NULL, &ov[i]);
    }
    
    if (WAIT_OBJECT_0 == WaitForMultipleObjects(N, hEvents, TRUE, INFINITE)) {
        puts("File successfully has been written!");
    }
    CloseHandle(hFile);

    char szBuf[N * M];
    DWORD dwRead;
    hFile = CreateFile(FILENAME, GENERIC_READ, FILE_SHARE_WRITE, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
    ReadFile(hFile, &szBuf, N*M, &dwRead, NULL);
    CloseHandle(hFile);

    // --------------------------
    //  V E R I F I C A T I O N
    // --------------------------
    CHAR linearArr[N * M];
    int index = 0;
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < M; j++, index++) {
            linearArr[index] = arr[i][j];
        }
    }

    puts( !strncmp(linearArr, szBuf, N * M) ? "Verification success" : "Verification failed" );
    
    getchar();
    return 0;
}
