#include <windows.h>
#include <stdio.h>

int main(int argc, char **argv) {
    size_t const VOLUME_NAME_SZ = 127;
    char szVolumeName[VOLUME_NAME_SZ];

    DWORD dwVolSerialNum;
    DWORD dwMaxComponentLen;

    DWORD dwSectorsPerCluster;
    DWORD dwBytesPerSector;
    DWORD dwFreeClusters;
    DWORD dwTotalClusters;

    GetVolumeInformation("C:\\", szVolumeName, VOLUME_NAME_SZ, &dwVolSerialNum, &dwMaxComponentLen, NULL, NULL, 0);
    GetDiskFreeSpace("C:\\", &dwSectorsPerCluster, &dwBytesPerSector, &dwFreeClusters, &dwTotalClusters);

    puts("------ DISK  C: info: ------");
    printf("Volume name = %s\n", szVolumeName);
    printf("Serial number = %04X-%04X\n", HIWORD(dwVolSerialNum), LOWORD(dwVolSerialNum));
    printf("Maximum component length = %lu\n", dwMaxComponentLen);
    puts("-----------------------------");
    printf("Sectors per cluster = %lu\n", dwSectorsPerCluster);
    printf("Bytes per sector = %lu\n", dwBytesPerSector);
    printf("Free clusters = %lu\n", dwFreeClusters);
    printf("Total clusters = %lu\n", dwTotalClusters);

    return 0;
}
