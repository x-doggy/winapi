#include <windows.h>
#include <stdio.h>

VOID MyReadOfMultiSz(LPCSTR lpszInMultiString) {
    for (; lpszInMultiString[0] != '\0'; lpszInMultiString += strlen(lpszInMultiString) + 1)
        puts(lpszInMultiString);
}

int main(int argc, char **argv) {
    HKEY  hKeyResult;
    LONG  lRes;
    DWORD dwDataSz;
    DWORD dwType;

    lRes = RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\Stadnik", 0L, KEY_QUERY_VALUE, &hKeyResult);

    CHAR lpszData[10];
    dwDataSz = 10;
    lRes = RegQueryValueEx(hKeyResult, "Name", NULL, &dwType, (CONST LPBYTE)lpszData, &dwDataSz);
    if (ERROR_SUCCESS == lRes) {
        printf("Name = %s\n", lpszData);
    }

    dwDataSz = sizeof(DWORD);
    DWORD lpAge[1];
    lRes = RegQueryValueEx(hKeyResult, "Age", NULL, &dwType, (CONST LPBYTE)lpAge, &dwDataSz);
    if (ERROR_SUCCESS == lRes) {
        printf("Age = %d\n", (INT)lpAge[0]);
    }

    CHAR lpMultiData[445];
    dwDataSz = 445;
    lRes = RegQueryValueEx(hKeyResult, "Lorem Ipsum", NULL, &dwType, (CONST LPBYTE)lpMultiData, &dwDataSz);
    if (ERROR_SUCCESS == lRes) {
        printf("%s\n", "Lorem Ipsum MultiSZ:");
        MyReadOfMultiSz(lpMultiData);
    }

    dwDataSz = sizeof(BYTE);
    BYTE lpNull[1];
    lRes = RegQueryValueEx(hKeyResult, "Null Key", NULL, &dwType, (CONST LPBYTE)lpNull, &dwDataSz);
    if (ERROR_SUCCESS == lRes) {
        printf("Binary NULL Key = %u\n", lpNull[0]);
    }

    RegCloseKey(hKeyResult);

    return 0;
}
