#include <windows.h>
#include <stdio.h>

CONST DWORD MySizeOfMultiSz(LPCSTR lpszInMultiString) {
    DWORD dwResult = 0;
    for (; lpszInMultiString[0] != '\0'; lpszInMultiString += strlen(lpszInMultiString) + 1)
        dwResult += strlen(lpszInMultiString) + 1;
    return dwResult;
}

int main(int argc, char **argv) {
    HKEY hKeyResult;
    LONG lRes;
    RegOpenKeyEx(HKEY_CURRENT_USER, "Software\\Stadnik", 0L, KEY_WRITE, &hKeyResult);

    LPCSTR pbNameData       = "Vladimir";
    CONST INT nAgeData      = 21;
    LPCSTR pbLoremIpsumData = "Lorem ipsum dolor sit amet, consectetur adipiscing "
                              "elit,\0sed do eiusmod tempor incididunt ut labore et "
                              "dolore magna aliqua.\0Ut enim ad minim veniam, quis "
                              "nostrud exercitation ullamco laboris nisi ut aliquip "
                              "ex ea commodo consequat.\0Duis aute irure dolor in "
                              "reprehenderit in voluptate velit esse cillum dolore "
                              "eu fugiat nulla pariatur.\0Excepteur\0sint\0occaecat "
                              "cupidatat non proident,\0sunt in culpa qui officia "
                              "deserunt mollit anim id est labor\0";

    lRes = RegSetValueEx(hKeyResult, "Name", 0, REG_SZ, (CONST LPBYTE)pbNameData, strlen(pbNameData));
    if (ERROR_SUCCESS == lRes) {
        puts("Key \"Name\" inserted successfully!");
    }

    lRes = RegSetValueEx(hKeyResult, "Age" , 0, REG_DWORD, (CONST LPBYTE)&nAgeData, sizeof(DWORD));
    if (ERROR_SUCCESS == lRes) {
        puts("Key \"Age\" inserted successfully!");
    }

    lRes = RegSetValueEx(hKeyResult, "Lorem Ipsum", 0, REG_MULTI_SZ, (CONST LPBYTE)pbLoremIpsumData, MySizeOfMultiSz(pbLoremIpsumData));
    if (ERROR_SUCCESS == lRes) {
        puts("Key \"Lorem Ipsum\" inserted successfully!");
    }

    lRes = RegSetValueEx(hKeyResult, "Null Key", 0, REG_BINARY, NULL, 0);
    if (ERROR_SUCCESS == lRes) {
        puts("Null Key inserted successfully!");
    }

    RegCloseKey(hKeyResult);

    return 0;
}
