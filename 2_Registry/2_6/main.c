#include <windows.h>
#include <stdio.h>

#define MAX_KEY_LENGTH 255
#define MAX_VALUE_NAME 16383

CONST DWORD MySizeOfMultiSz(LPCSTR lpszInMultiString) {
    DWORD dwResult = 0;
    for (; lpszInMultiString[0] != '\0'; lpszInMultiString += strlen(lpszInMultiString) + 1)
        dwResult += strlen(lpszInMultiString) + 1;
    return dwResult;
}

VOID MyReadOfMultiSz(LPCSTR lpszInMultiString) {
    for (; lpszInMultiString[0] != '\0'; lpszInMultiString += strlen(lpszInMultiString) + 1)
        puts(lpszInMultiString);
}

void QueryKey(HKEY hKey) {
    CHAR     achKey[MAX_KEY_LENGTH];   // buffer for subkey name
    DWORD    cbName;                   // size of name string
    CHAR     achClass[MAX_PATH] = "";  // buffer for class name
    DWORD    cchClassName = MAX_PATH;  // size of class string
    DWORD    cSubKeys=0;               // number of subkeys
    DWORD    cbMaxSubKey;              // longest subkey size
    DWORD    cchMaxClass;              // longest class string
    DWORD    cValues;              // number of values for key
    DWORD    cchMaxValue;          // longest value name
    DWORD    cbMaxValueData;       // longest value data
    DWORD    cbSecurityDescriptor; // size of security descriptor
    FILETIME ftLastWriteTime;      // last write time

    DWORD i, retCode;

    CHAR  achValue[MAX_VALUE_NAME];
    DWORD cchValue = MAX_VALUE_NAME;

    // Get the class name and the value count.
    retCode = RegQueryInfoKey(
        hKey,                    // key handle
        achClass,                // buffer for class name
        &cchClassName,           // size of class string
        NULL,                    // reserved
        &cSubKeys,               // number of subkeys
        &cbMaxSubKey,            // longest subkey size
        &cchMaxClass,            // longest class string
        &cValues,                // number of values for this key
        &cchMaxValue,            // longest value name
        &cbMaxValueData,         // longest value data
        &cbSecurityDescriptor,   // security descriptor
        &ftLastWriteTime);       // last write time

    // Enumerate the subkeys, until RegEnumKeyEx fails.

    if (cSubKeys) {
        printf("\nNumber of subkeys: %d\n", cSubKeys);
        for (i=0; i<cSubKeys; i++) {
            cbName = MAX_KEY_LENGTH;
            retCode = RegEnumKeyEx(hKey, i, achKey, &cbName, NULL, NULL, NULL, &ftLastWriteTime);
            if (ERROR_SUCCESS == retCode) {
                printf("(%d) %s\n", i+1, achKey);
            }
        }
    }

    // Enumerate the key values.

    if (cValues) {
        printf("\nNumber of values: %d\n", cValues);

        for (i=0, retCode=ERROR_SUCCESS; i<cValues; i++) {
            cchValue = MAX_VALUE_NAME;
            achValue[0] = '\0';
            retCode = RegEnumValue(hKey, i, achValue, &cchValue, NULL, NULL, NULL, NULL);
            if (ERROR_SUCCESS == retCode) {
                printf("(%d) %s\n", i+1, achValue);
            }
        }
    }
}


int main(int argc, char **argv) {
    HKEY hKeyResult;
    LONG lRes;

    // 1.
    lRes = RegCreateKeyEx(HKEY_LOCAL_MACHINE, "Software\\Stadnik", 0, NULL, REG_OPTION_VOLATILE, KEY_WRITE, NULL, &hKeyResult, NULL);

    if (ERROR_SUCCESS == lRes) {
        puts("Regkey created successfully!");
    }

    RegCloseKey(hKeyResult);

    // 2.
    RegOpenKeyEx(HKEY_LOCAL_MACHINE, "Software\\Stadnik", 0L, KEY_WRITE, &hKeyResult);

    LPCSTR pbNameData       = "Vladimir";
    CONST INT      nAgeData = 21;
    LPCSTR pbLoremIpsumData = "Lorem ipsum dolor sit amet, consectetur adipiscing "
                              "elit,\0sed do eiusmod tempor incididunt ut labore et "
                              "dolore magna aliqua.\0Ut enim ad minim veniam, quis "
                              "nostrud exercitation ullamco laboris nisi ut aliquip "
                              "ex ea commodo consequat.\0Duis aute irure dolor in "
                              "reprehenderit in voluptate velit esse cillum dolore "
                              "eu fugiat nulla pariatur.\0Excepteur\0sint\0occaecat "
                              "cupidatat non proident,\0sunt in culpa qui officia "
                              "deserunt mollit anim id est labor\0";

    lRes = RegSetValueEx(hKeyResult, "Name", 0, REG_SZ, (CONST LPBYTE)pbNameData, strlen(pbNameData));
    if (ERROR_SUCCESS == lRes) {
        puts("Key \"Name\" inserted successfully!");
    }

    lRes = RegSetValueEx(hKeyResult, "Age" , 0, REG_DWORD, (CONST LPBYTE)&nAgeData, sizeof(DWORD));
    if (ERROR_SUCCESS == lRes) {
        puts("Key \"Age\" inserted successfully!");
    }

    lRes = RegSetValueEx(hKeyResult, "Lorem Ipsum", 0, REG_MULTI_SZ, (CONST LPBYTE)pbLoremIpsumData, MySizeOfMultiSz(pbLoremIpsumData));
    if (ERROR_SUCCESS == lRes) {
        puts("Key \"Lorem Ipsum\" inserted successfully!");
    }

    lRes = RegSetValueEx(hKeyResult, "Null Key", 0, REG_BINARY, NULL, 0);
    if (ERROR_SUCCESS == lRes) {
        puts("Null Key inserted successfully!");
    }

    // 3.
    DWORD dwDataSz;
    DWORD dwType;

    CHAR lpszData[10];
    dwDataSz = 10;
    lRes = RegQueryValueEx(hKeyResult, "Name", NULL, &dwType, (CONST LPBYTE)lpszData, &dwDataSz);
    if (ERROR_SUCCESS == lRes) {
        printf("Name = %s\n", lpszData);
    }

    dwDataSz = sizeof(DWORD);
    DWORD lpAge[1];
    lRes = RegQueryValueEx(hKeyResult, "Age", NULL, &dwType, (CONST LPBYTE)lpAge, &dwDataSz);
    if (ERROR_SUCCESS == lRes) {
        printf("Age = %d\n", (INT)lpAge[0]);
    }

    CHAR lpMultiData[445];
    dwDataSz = 445;
    lRes = RegQueryValueEx(hKeyResult, "Lorem Ipsum", NULL, &dwType, (CONST LPBYTE)lpMultiData, &dwDataSz);
    if (ERROR_SUCCESS == lRes) {
        puts("Lorem Ipsum MultiSZ:");
        MyReadOfMultiSz(lpMultiData);
    }

    dwDataSz = sizeof(BYTE);
    BYTE lpNull[1];
    lRes = RegQueryValueEx(hKeyResult, "Null Key", NULL, &dwType, (CONST LPBYTE)lpNull, &dwDataSz);
    if (ERROR_SUCCESS == lRes) {
        printf("Binary NULL Key = %u\n", lpNull[0]);
    }

    // 4.
    QueryKey(hKeyResult);

    RegCloseKey(hKeyResult);

    return 0;
}
