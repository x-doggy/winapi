#include <windows.h>
#include <stdio.h>

int main(int argc, char **argv) {
    HKEY hKeyResult;
    LONG lRes = RegCreateKeyEx(HKEY_CURRENT_USER, "Software\\Stadnik", 0, NULL, REG_OPTION_VOLATILE, KEY_WRITE, NULL, &hKeyResult, NULL);

    if (ERROR_SUCCESS == lRes) {
        puts("Regkey created successfully!");
    }

    RegCloseKey(hKeyResult);

    return 0;
}
