#include <windows.h>
#include <stdio.h>

int main(int argc, char **argv) {
    STARTUPINFO si = { 0 };
    si.cb = sizeof(si);
    PROCESS_INFORMATION pi = { 0 };

    if (argc != 2) {
        fprintf(stderr, "Usage: %s [cmdline]\n", argv[0]);
        getchar();
        return 0;
    }

    if (!CreateProcess(NULL, argv[1], NULL, NULL, FALSE, NULL, NULL, NULL, &si, &pi)) {
        fprintf(stderr, "CreateProcess failed with error code %lu.\n", GetLastError());
        getchar();
        return -1;
    }
    printf("Process with command line \"%s\" created successfully!\n", argv[1]);
    printf("Process's process handle: %p\n", pi.hProcess);
    printf("Process's thread handle: %p\n" , pi.hThread);
    printf("Process's process ID: %lu\n"   , pi.dwProcessId);
    printf("Process's thread ID: %lu\n"    , pi.dwThreadId);

    CloseHandle(pi.hThread);
    if (WAIT_TIMEOUT == WaitForSingleObject(pi.hProcess, 5000)) {
        if (!TerminateProcess(pi.hProcess, 0)) {
            fprintf(stderr, "%s\n", "Process terminating failed!");
            getchar();
            return -1;
        }
    }
    CloseHandle(pi.hProcess);
    printf("Process with command line \"%s\" terminated successfully!\n", argv[1]);

    getchar();
    return 0;
}
