#include <windows.h>
#include <stdio.h>

int main(int argc, char **argv) {
    STARTUPINFO si = { 0 };
    si.cb = sizeof(si);
    PROCESS_INFORMATION pi = { 0 };

    if (argc != 2) {
        printf("Usage: %s [cmdline]\n", argv[0]);
        return 0;
    }

    if (CreateProcess(NULL, argv[1], NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi)) {
        printf("Process with command line \"%s\" created successfully!\n", argv[1]);
        printf("Process's process handle: %p\n", pi.hProcess);
        printf("Process's thread handle: %p\n" , pi.hThread);
        printf("Process's process ID: %u\n"    , pi.dwProcessId);
        printf("Process's thread ID: %u\n"     , pi.dwThreadId);

        if (WAIT_OBJECT_0 == WaitForSingleObject(pi.hProcess, INFINITE)) {
            CloseHandle(pi.hThread);
            CloseHandle(pi.hProcess);
        } else {
           fprintf(stderr, "Something's goes wrong with errcode %d!", GetLastError());
        }
    } else {
        fprintf(stderr, "CreateProcess failed with error code (%u).\n", GetLastError());
    }

    return 0;
}
